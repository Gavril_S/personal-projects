class Service:
    def __init__(self, service_id):
        self.service_id = service_id

    def dump(self):
        return {'service_id': self.service_id}