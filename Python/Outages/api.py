from Input import get_input
from datetime import datetime, timedelta
from Service import Service
from Outage import Outage
from AccumulatedOutages import AccumulatedOutages
import json
import hug

input = get_input()


@hug.get('/down')
@hug.local()
def currently_down_services():
    """Get Currently Down Services"""
    time = datetime.now()
    services = []
    flag = False
    for outage in input:
        if outage.start_time <= time and outage.end_time >= time:
            service = Outage(outage.service_id, outage.start_time.strftime('%Y-%m-%d %H:%M:%S'), outage.duration)
            for s in services:
                if s.service_id == service.service_id:
                    flag = True
                    break

            if not flag:
                services.append(service)
            flag = False
    result = json.dumps([o.dump() for o in services])
    return result


@hug.get('/recent')
def recently_down_services():
    """Get Recently Down Services"""
    time = datetime.now()
    recent_time = datetime.now() - timedelta(seconds=3600)
    services = []
    flag = False
    for outage in input:
        if outage.end_time >= recent_time and outage.end_time < time:
            service = Outage(outage.service_id, outage.start_time.strftime('%Y-%m-%d %H:%M:%S'), outage.duration)
            for s in services:
                if s.service_id == service.service_id:
                    flag = True
                    break

            if not flag:
                services.append(service)
            flag = False
    result = json.dumps([o.dump() for o in services])
    return result


@hug.get('/add_outage')
def add_outage(service_id: hug.types.text, start_time: hug.types.text, duration: hug.types.text):
    """Add An Outage For A Service"""
    outage = Outage(service_id, start_time, duration)
    input.append(outage)
    return outage.dump()


def calculate_service_down_time(custom_inp, service_id):
    filtered_list = []
    for inp in custom_inp:
        if inp.service_id == service_id:
            filtered_list.append(inp)
    filtered_list.sort(key=lambda x: x.start_time)
    accumulated_outages = []
    amount_of_outages = 0
    outages_sum = 0
    for item in filtered_list:
        start_time = item.start_time
        end_time = item.start_time + timedelta(seconds=7200)
        for i in filtered_list:
            if i.start_time >= start_time and i.end_time <= end_time:
                outages_sum = outages_sum + i.duration
                amount_of_outages = amount_of_outages + 1
        if outages_sum >= 900:
            service_outage = AccumulatedOutages(service_id, start_time, outages_sum, end_time, amount_of_outages,
                                                outages_sum)
            accumulated_outages.append(service_outage)
        amount_of_outages = 0
        outages_sum = 0
    return accumulated_outages


@hug.get('/acc_time')
def get_accumulated_services_down_time():
    """Get Flapping Scenarios For Services"""
    accum_outages = []
    outages_per_service = []
    services = []
    flag = False
    for i in input:
        for s in services:
            if s.service_id == i.service_id:
                flag = True
                break
        if not flag:
            service = Service(i.service_id)
            services.append(service)
        flag = False

    for s in services:
        outages_per_service = calculate_service_down_time(input, s.service_id)
        for out in outages_per_service:
            service_accum_outages = AccumulatedOutages(out.service_id, out.start_time, out.sum_of_outages, out.end_time,
                                                       out.amount_of_outages, out.sum_of_outages)
            accum_outages.append(service_accum_outages)
    result = json.dumps([o.dump() for o in accum_outages])
    return result
