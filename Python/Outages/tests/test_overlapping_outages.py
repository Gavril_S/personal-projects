import unittest
from Outage import Outage
from OverlappingOutages import OverlappingOutages

class TestOverlappingOutages(unittest.TestCase):

    def test_it_will_return_two_outages_from_custom_input(self):
        #Assume
        outage1 = Outage('4433', '2020-08-05 12:30:20', '350')
        outage2 = Outage('4433', '2020-08-05 12:50:14', '900')
        custom_inp = []
        custom_inp.append(outage1)
        custom_inp.append(outage2)
        #Action
        result = OverlappingOutages.calculate_service_down_time(custom_inp, '4433')
        checker = False
        if result[0].amount_of_outages != 2:
            checker = True
        #Assert
        self.assertFalse(checker)

    def test_will_return_same_duration_of_outages(self):
        # Assume
        outage1 = Outage('4433', '2020-08-05 12:30:20', '350')
        outage2 = Outage('4433', '2020-08-05 12:50:14', '900')
        custom_inp = []
        custom_inp.append(outage1)
        custom_inp.append(outage2)
        # Action
        result = OverlappingOutages.calculate_service_down_time(custom_inp, '4433')
        checker = False
        if result[0].duration != 1250:
            checker = True
        #Assert
        self.assertFalse(checker)

    def test_will_return_no_outages(self):
        # Assume
        outage1 = Outage('4433', '2020-08-05 12:30:20', '350')
        outage2 = Outage('4421', '2020-08-05 12:50:14', '800')
        custom_inp = []
        custom_inp.append(outage1)
        custom_inp.append(outage2)
        # Action
        result = OverlappingOutages.calculate_service_down_time(custom_inp, '4433')
        checker = False
        if len(result) > 0:
            checker = True
        # Assert
        self.assertFalse(checker)