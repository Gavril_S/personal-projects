class AccumulatedOutages:
    def __init__(self, service_id, start_time, duration, end_time, amount_of_outages, sum_of_outages):
        self.service_id = service_id
        self.start_time = start_time
        self.duration = duration
        self.end_time = end_time
        self.amount_of_outages = amount_of_outages
        self.sum_of_outages = sum_of_outages

    def dump(self):
        return {'service_id': self.service_id,
                'start_time': self.start_time.strftime('%Y-%m-%d %H:%M:%S'),
                'duration': self.duration,
                'end_time': self.end_time.strftime('%Y-%m-%d %H:%M:%S'),
                'amount_of_outages': self.amount_of_outages,
                'sum_of_outages': self.sum_of_outages}
