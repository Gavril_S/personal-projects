import json
from Outage import Outage

def get_input():
    with open('outages.txt') as f:
        data = json.load(f)
        outages = []

        i = 0
        for d in data:
            outage = Outage(d["service_id"], d["startTime"], d["duration"])
            outages.append(outage)

        return outages
