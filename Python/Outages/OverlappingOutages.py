from datetime import timedelta
from AccumulatedOutages import AccumulatedOutages

class OverlappingOutages:
    def calculate_service_down_time(custom_inp, service_id):
        filtered_list = []
        for inp in custom_inp:
            if inp.service_id == service_id:
                filtered_list.append(inp)
        filtered_list.sort(key=lambda x: x.start_time)
        accumulated_outages = []
        amount_of_outages = 0
        outages_sum = 0
        for item in filtered_list:
            start_time = item.start_time
            end_time = item.start_time + timedelta(seconds=7200)
            for i in filtered_list:
                if i.start_time >= start_time and i.end_time <= end_time:
                    outages_sum = outages_sum + i.duration
                    amount_of_outages = amount_of_outages + 1
            if outages_sum >= 900:
                service_outage = AccumulatedOutages(service_id, start_time, outages_sum, end_time, amount_of_outages,
                                                    outages_sum)
                accumulated_outages.append(service_outage)
            amount_of_outages = 0
            outages_sum = 0
        return accumulated_outages