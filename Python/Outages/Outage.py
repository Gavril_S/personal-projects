from datetime import datetime, timedelta


class Outage:
    def __init__(self, service_id, start_time, duration):
        self.service_id = service_id
        self.start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
        self.duration = int(duration)
        self.end_time = self.calculate_end_time(start_time, int(duration))

    def calculate_end_time(self, start_time, duration):
        datetime_obj = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
        end_time = datetime_obj + timedelta(seconds=duration)
        return end_time

    def dump(self):
        return {'service_id': self.service_id,
                'start_time': self.start_time.strftime('%Y-%m-%d %H:%M:%S'),
                'duration': self.duration,
                'end_time': self.end_time.strftime('%Y-%m-%d %H:%M:%S')}
