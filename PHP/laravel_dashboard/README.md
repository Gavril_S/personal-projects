PHP/laravel Dashboard Task:

A simple dashboard site with registration and authentication, where users can create, update and delete shortcuts to websites.

Tech stack: PHP 7.4, Laravel 8.x, HTML 5, CSS, JavaScript, MySQL.

How to run:
Simply clone the project to your system, open a command terminal and enter the command: "php artisan serve". Click on the link that is given by the command terminal.

Heroku deployment: laravel-dashboard-task.herokuapp.com
