package com.project.beer_tag.service.contracts;

import com.project.beer_tag.models.Country;

import java.util.List;

public interface CountryService {

    List<Country> getCountries();

    Country getCountryByName(String name);

    Country createCountry(String name);

    Country setCountryFromString(String country);
}
