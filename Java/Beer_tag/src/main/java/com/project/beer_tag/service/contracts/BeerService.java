package com.project.beer_tag.service.contracts;

import com.project.beer_tag.models.Beer;
import com.project.beer_tag.models.dto.BeerDto;

import java.util.List;

public interface BeerService {
    List<Beer> getBeers();

    Beer getBeerByName(String name);

    Beer saveBeer(BeerDto beer);

    Beer updateBeer(BeerDto beer, int beerToUpdateId);

    Beer deleteBeer(String name);

    List<Beer> filterBeers();

    List<Beer> sortBeers();
}
