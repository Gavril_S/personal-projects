package com.project.beer_tag.repository;

import com.project.beer_tag.models.Brewery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BreweryRepository extends JpaRepository<Brewery, Integer> {

    Brewery findBreweryByName(String name);
}
