package com.project.beer_tag.controllers.rest;

import com.project.beer_tag.exceptions.EntityNotFoundException;
import com.project.beer_tag.models.Beer;
import com.project.beer_tag.models.dto.BeerDto;
import com.project.beer_tag.service.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/beers")
public class BeerRestController {
    private BeerService beerService;

    @Autowired
    public BeerRestController(BeerService beerService) {
        this.beerService = beerService;
    }

    @GetMapping("/{name}")
    public Beer getBeerByName(@PathVariable String name) {
        return beerService.getBeerByName(name);
    }

    @GetMapping
    public List<Beer> getBeers() {
        return beerService.getBeers();
    }

    @PostMapping
    public Beer createBeer(@RequestParam BeerDto beerDto) throws EntityNotFoundException {
        return beerService.saveBeer(beerDto);
    }

    @PutMapping("/{id}")
    public Beer updateBeer(@RequestParam BeerDto beerDto, @PathVariable int id) throws EntityNotFoundException {
        return beerService.updateBeer(beerDto, id);
    }

    @DeleteMapping
    public Beer deleteBeer(@RequestParam String name) throws EntityNotFoundException {
        return beerService.deleteBeer(name);
    }
}
