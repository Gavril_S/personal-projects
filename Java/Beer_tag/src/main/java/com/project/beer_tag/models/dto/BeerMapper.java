package com.project.beer_tag.models.dto;

import com.project.beer_tag.models.Beer;

public class BeerMapper {

    public static Beer beerToDtoMapper(BeerDto beer) {
        Beer newBeer = new Beer();
        newBeer.setName(beer.getName());
        newBeer.setDescription(beer.getDescription());
        newBeer.setAbv(beer.getAbv());
        newBeer.setPicture(beer.getPicture());
        return newBeer;
    }
}
