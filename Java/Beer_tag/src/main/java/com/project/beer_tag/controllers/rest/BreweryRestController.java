package com.project.beer_tag.controllers.rest;

import com.project.beer_tag.models.Brewery;
import com.project.beer_tag.service.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweryRestController {
    private BreweryService breweryService;

    @Autowired
    public BreweryRestController(BreweryService breweryService) {
        this.breweryService = breweryService;
    }

    @GetMapping("/{name}")
    public Brewery getBreweryByName(@PathVariable String name) {
        return breweryService.getBreweryByName(name);
    }

    @GetMapping
    public List<Brewery> getBreweries() {
        return breweryService.getBreweries();
    }

    @PostMapping()
    public Brewery createBrewery(@RequestParam String name) {
        return breweryService.createBrewery(name);
    }
}
