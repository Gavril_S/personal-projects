package com.project.beer_tag.controllers.rest;

import com.project.beer_tag.models.Style;
import com.project.beer_tag.service.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StyleRestController {
    private StyleService styleService;

    @Autowired
    public StyleRestController(StyleService styleService) {
        this.styleService = styleService;
    }

    @GetMapping("/{name}")
    public Style getStyleByName(@PathVariable String name) {
        return styleService.getStyleByName(name);
    }

    @GetMapping
    public List<Style> getStyles() {
        return styleService.getStyles();
    }

    @PostMapping
    public Style createStyle(@RequestParam String name) {
        return styleService.createStyle(name);
    }
}
