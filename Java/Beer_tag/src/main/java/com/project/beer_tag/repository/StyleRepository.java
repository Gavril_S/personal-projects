package com.project.beer_tag.repository;

import com.project.beer_tag.models.Style;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StyleRepository extends JpaRepository<Style, Integer> {

    Style findStyleByName(String name);
}
