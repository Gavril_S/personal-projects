package com.project.beer_tag.service.contracts;

import com.project.beer_tag.models.Brewery;

import java.util.List;

public interface BreweryService {

    List<Brewery> getBreweries();

    Brewery getBreweryByName(String breweryName);

    Brewery createBrewery(String breweryName);

    Brewery setBreweryFromString(String brewery);
}
