package com.project.beer_tag.repository;

import com.project.beer_tag.models.Country;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

    Country findCountryByName(String name);
}
