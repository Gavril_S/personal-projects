package com.project.beer_tag.exceptions;

public class EntityNotFoundException extends RuntimeException {

    private static final String ERROR_MSG = "A %s with that name and id does not exist! " +
                                    "Either create it or choose one that already exists!";

    public EntityNotFoundException(String entityType){
        super(String.format(ERROR_MSG, entityType));
    }
}
