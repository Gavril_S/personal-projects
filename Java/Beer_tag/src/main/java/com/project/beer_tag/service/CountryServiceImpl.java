package com.project.beer_tag.service;

import com.project.beer_tag.exceptions.EntityNotFoundException;
import com.project.beer_tag.models.Country;
import com.project.beer_tag.repository.CountryRepository;
import com.project.beer_tag.service.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService {
    private static final String ERROR_ENTITY = "country";

    private CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public List<Country> getCountries() {
        return countryRepository.findAll();
    }

    @Override
    public Country getCountryByName(String name) {
        return countryRepository.findCountryByName(name);
    }

    @Override
    public Country createCountry(String name) {
        Country country = new Country();
        country.setName(name);
        countryRepository.save(country);
        return country;
    }

    @Override
    public Country setCountryFromString(String country) {
        Country newCountry = getCountryByName(country);
        if(newCountry == null) {
            throw new EntityNotFoundException(ERROR_ENTITY);
        }
        return newCountry;
    }
}
