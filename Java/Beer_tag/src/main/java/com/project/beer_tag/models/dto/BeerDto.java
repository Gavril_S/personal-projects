package com.project.beer_tag.models.dto;

public class BeerDto {

    private String name;
    private String description;
    private double abv;
    private String styleName;
    private String countryName;
    private String breweryName;
    private String tags;
    private String picture;

    public BeerDto() {
    }

    public BeerDto(String name, String description, double abv, String styleName, String countryName,
                        String breweryName, String tags, String picture) {
        this.name = name;
        this.description = description;
        this.abv = abv;
        this.styleName = styleName;
        this.countryName = countryName;
        this.breweryName = breweryName;
        this.tags = tags;
        this.picture = picture;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getStyleName() {
        return styleName;
    }

    public void setStyleName(String styleName) {
        this.styleName = styleName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getBreweryName() {
        return breweryName;
    }

    public void setBreweryName(String breweryName) {
        this.breweryName = breweryName;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }
}
