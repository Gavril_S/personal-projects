package com.project.beer_tag.service;

import com.project.beer_tag.exceptions.EntityNotFoundException;
import com.project.beer_tag.models.*;
import com.project.beer_tag.models.dto.BeerDto;
import com.project.beer_tag.models.dto.BeerMapper;
import com.project.beer_tag.repository.BeerRepository;
import com.project.beer_tag.service.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
public class BeerServiceImpl implements BeerService {
    private static final String ERROR_ENTITY = "beer";

    private BeerRepository beerRepository;
    private BreweryService breweryService;
    private CountryService countryService;
    private StyleService styleService;
    private TagService tagService;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository, BreweryService breweryService, CountryService countryService,
                                    StyleService styleService, TagService tagService) {
        this.beerRepository = beerRepository;
        this.breweryService = breweryService;
        this.countryService = countryService;
        this.styleService = styleService;
        this.tagService = tagService;
    }

    @Override
    public List<Beer> getBeers() {
        return beerRepository.findAll();
    }

    @Override
    public Beer getBeerByName(String name) {
        return beerRepository.findBeerByName(name);
    }

    @Override
    public Beer saveBeer(BeerDto beer) throws EntityNotFoundException {
        Beer newBeer = getBeerFromDto(beer);
        beerRepository.save(newBeer);
        return newBeer;
    }

    @Override
    public Beer updateBeer(BeerDto beer, int beerToUpdateId) throws EntityNotFoundException {
        Beer oldBeer = beerRepository.getById(beerToUpdateId);
        if(oldBeer == null) {
            throw new EntityNotFoundException(ERROR_ENTITY);
        }
        Beer newBeer = getBeerFromDto(beer);
        newBeer.setId(oldBeer.getId());
        beerRepository.delete(oldBeer);
        beerRepository.save(newBeer);
        return newBeer;
    }

    private Beer getBeerFromDto(BeerDto beer) throws EntityNotFoundException {
        Beer newBeer;
        newBeer = BeerMapper.beerToDtoMapper(beer);
        Brewery brewery = breweryService.setBreweryFromString(beer.getBreweryName());
        newBeer.setBrewery(brewery);
        Country country = countryService.setCountryFromString(beer.getCountryName());
        newBeer.setOrigin_country(country);
        Style style = styleService.setStyleFromString(beer.getStyleName());
        newBeer.setStyle(style);
        Set<Tag> tags = tagService.setTagsFromString(beer.getTags());
        newBeer.setTags(tags);
        return newBeer;
    }

    @Override
    public Beer deleteBeer(String name) {
        Beer beerToDelete = getBeerByName(name);
        if(beerToDelete == null) {
            throw new EntityNotFoundException(ERROR_ENTITY);
        }
        beerRepository.delete(beerToDelete);
        return beerToDelete;
    }

    @Override
    public List<Beer> filterBeers() {
        return null;
    }

    @Override
    public List<Beer> sortBeers() {
        return null;
    }
}
