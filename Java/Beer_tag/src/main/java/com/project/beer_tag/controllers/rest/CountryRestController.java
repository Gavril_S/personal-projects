package com.project.beer_tag.controllers.rest;

import com.project.beer_tag.models.Country;
import com.project.beer_tag.service.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountryRestController {
    private CountryService countryService;

    @Autowired
    public CountryRestController(CountryService countryService) {
        this.countryService = countryService;
    }

    @GetMapping("/{name}")
    public Country getCountryByName(@PathVariable String name) {
        return countryService.getCountryByName(name);
    }

    @GetMapping
    public List<Country> getCountries() {
        return countryService.getCountries();
    }

    @PostMapping
    public Country createCountry(@RequestParam String name) {
        return countryService.createCountry(name);
    }
}
