package com.project.beer_tag.service.contracts;

import com.project.beer_tag.models.Style;

import java.util.List;

public interface StyleService {

    List<Style> getStyles();

    Style getStyleByName(String styleName);

    Style createStyle(String styleName);

    Style setStyleFromString(String style);
}
