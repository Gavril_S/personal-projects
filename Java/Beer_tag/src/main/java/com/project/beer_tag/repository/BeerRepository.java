package com.project.beer_tag.repository;

import com.project.beer_tag.models.Beer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BeerRepository extends JpaRepository<Beer, Integer> {
    Beer findBeerByName(String name);

    Beer getById(int id);
}
