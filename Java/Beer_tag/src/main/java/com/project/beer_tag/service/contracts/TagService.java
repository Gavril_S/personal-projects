package com.project.beer_tag.service.contracts;

import com.project.beer_tag.models.Tag;

import java.util.List;
import java.util.Set;

public interface TagService {

    List<Tag> getTags();

    Tag getTagByName(String tagName);

    Tag createTag(String tagName);

    Set<Tag> setTagsFromString(String tags);
}
