package com.project.beer_tag.service;

import com.project.beer_tag.exceptions.EntityNotFoundException;
import com.project.beer_tag.models.Brewery;
import com.project.beer_tag.repository.BreweryRepository;
import com.project.beer_tag.service.contracts.BreweryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BreweryServiceImpl implements BreweryService {
    private static final String ERROR_ENTITY = "brewery";

    private BreweryRepository breweryRepository;

    @Autowired
    public BreweryServiceImpl(BreweryRepository breweryRepository) {
        this.breweryRepository = breweryRepository;
    }

    @Override
    public List<Brewery> getBreweries() {
        return breweryRepository.findAll();
    }

    @Override
    public Brewery getBreweryByName(String breweryName) {
        return breweryRepository.findBreweryByName(breweryName);
    }

    @Override
    public Brewery createBrewery(String breweryName) {
        Brewery brewery = new Brewery();
        brewery.setName(breweryName);
        breweryRepository.save(brewery);
        return brewery;
    }

    @Override
    public Brewery setBreweryFromString(String brewery) {
        Brewery newBrewery = getBreweryByName(brewery);
        if(newBrewery == null) {
            throw new EntityNotFoundException(ERROR_ENTITY);
        }
        return newBrewery;
    }
}
