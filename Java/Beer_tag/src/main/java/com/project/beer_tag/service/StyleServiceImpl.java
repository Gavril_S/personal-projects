package com.project.beer_tag.service;

import com.project.beer_tag.exceptions.EntityNotFoundException;
import com.project.beer_tag.models.Style;
import com.project.beer_tag.repository.StyleRepository;
import com.project.beer_tag.service.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private static final String ERROR_ENTITY = "style";

    private StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getStyles() {
        return styleRepository.findAll();
    }

    @Override
    public Style getStyleByName(String styleName) {
        return styleRepository.findStyleByName(styleName);
    }

    @Override
    public Style createStyle(String styleName) {
        Style style = new Style();
        style.setName(styleName);
        styleRepository.save(style);
        return style;
    }

    @Override
    public Style setStyleFromString(String style) {
        Style newStyle = getStyleByName(style);
        if(newStyle == null) {
            throw new EntityNotFoundException(ERROR_ENTITY);
        }
        return newStyle;
    }
}
