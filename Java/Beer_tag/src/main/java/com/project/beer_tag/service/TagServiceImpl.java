package com.project.beer_tag.service;

import com.project.beer_tag.models.Tag;
import com.project.beer_tag.repository.TagRepository;
import com.project.beer_tag.service.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class TagServiceImpl implements TagService {

    private TagRepository tagRepository;

    @Autowired
    public TagServiceImpl(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    @Override
    public List<Tag> getTags() {
        return tagRepository.findAll();
    }

    @Override
    public Tag getTagByName(String tagName) {
        return tagRepository.findTagByName(tagName);
    }

    @Override
    public Tag createTag(String tagName) {
        Tag tag = new Tag();
        tag.setName(tagName);
        tagRepository.save(tag);
        return tag;
    }

    @Override
    public Set<Tag> setTagsFromString(String tags) {
        Set<Tag> tagSet = new HashSet<>();
        String[] tagArr = tags.split("[, #.!?@:;]+");
        Tag oldTag;
        for (String tag : tagArr) {
            oldTag = getTagByName(tag);
            if(oldTag != null) {
                tagSet.add(oldTag);
            } else {
                Tag newTag = new Tag();
                newTag.setName(tag);
                createTag(newTag.getName());
                tagSet.add(newTag);
            }
        }
        return tagSet;
    }
}
