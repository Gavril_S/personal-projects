package com.project.beer_tag.repository;

import com.project.beer_tag.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {

    Tag findTagByName(String name);
}
