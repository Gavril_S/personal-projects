package com.project.beer_tag.controllers.rest;

import com.project.beer_tag.models.Tag;
import com.project.beer_tag.service.contracts.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagRestController {
    private TagService tagService;

    @Autowired
    public TagRestController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping("/{name}")
    public Tag getTagByName(@PathVariable String name) {
        return tagService.getTagByName(name);
    }

    @GetMapping
    public List<Tag> getTags() {
        return tagService.getTags();
    }

    @PostMapping
    public Tag createTag(@RequestParam String name) {
        return tagService.createTag(name);
    }
}
