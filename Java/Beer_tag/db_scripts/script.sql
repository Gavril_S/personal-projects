create table beers
(
    beer_id        int auto_increment,
    name           varchar(200)     null,
    description    varchar(200)     not null,
    abv            double default 0 not null,
    style          int    default 0 not null,
    origin_country int    default 0 not null,
    brewery        int    default 0 not null,
    tags           int    default 0 not null,
    rating         double default 0 not null,
    picture        blob             null,
    constraint beers_id_uindex
        unique (beer_id),
    constraint brewery
        unique (brewery),
    constraint id
        unique (beer_id),
    constraint id_uindex
        unique (beer_id),
    constraint origin_country
        unique (origin_country),
    constraint style
        unique (style)
);

create table brewery
(
    brewery_id int          not null
        primary key,
    name       varchar(200) null,
    constraint brewery_beers_brewery_fk
        foreign key (brewery_id) references beers (brewery)
);

alter table beers
    add constraint beers_brewery_brewery_id_fk
        foreign key (brewery) references brewery (brewery_id);

create table country
(
    country_id int          not null
        primary key,
    name       varchar(200) null,
    constraint country_beers_origin_country_fk
        foreign key (country_id) references beers (origin_country)
);

alter table beers
    add constraint beers_country_country_id_fk
        foreign key (origin_country) references country (country_id);

create table style
(
    style_id int         not null
        primary key,
    name     varchar(50) null,
    constraint style_beers_style_fk
        foreign key (style_id) references beers (style)
);

alter table beers
    add constraint beers_style_style_id_fk
        foreign key (style) references style (style_id);

create table users
(
    id          int                     not null,
    name        varchar(200) default '' not null,
    email       varchar(200) default '' not null,
    profile_pic blob         default '' not null
);


